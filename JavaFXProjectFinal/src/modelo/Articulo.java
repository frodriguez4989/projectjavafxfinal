package modelo;

import java.util.ArrayList;
import java.util.List;

public class Articulo {

	private Integer codigoArticulo;

	private String descripcion;

	private List<CodigoBarras> listaCodigoBarras;
	
	

	public Articulo() {
		listaCodigoBarras = new ArrayList<>();
	}

	public Integer getCodigoArticulo() {
		return codigoArticulo;
	}

	public void setCodigoArticulo(Integer codigoArticulo) {
		this.codigoArticulo = codigoArticulo;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}


	public List<CodigoBarras> getListaCodigoBarras() {
		return listaCodigoBarras;
	}

	public void setListaCodigoBarras(List<CodigoBarras> listaCodigoBarras) {
		this.listaCodigoBarras = listaCodigoBarras;
	}

	@Override
	public String toString() {
		return "Articulo [codigoArticulo=" + codigoArticulo + ", descripcion=" + descripcion + ", listaCodigoBarras="
				+ listaCodigoBarras + "]";
	}

}
