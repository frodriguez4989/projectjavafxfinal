package modelo;


public class CodigoBarras {
	
	private Integer codigo;
	
	private String talla;
	
	private String color;
	
	private Integer stockMax;

	public Integer getCodigo() {
		return codigo;
	}

	public void setCodigo(Integer codigo) {
		this.codigo = codigo;
	}

	public String getTalla() {
		return talla;
	}

	public void setTalla(String talla) {
		this.talla = talla;
	}

	public String getColor() {
		return color;
	}

	public void setColor(String color) {
		this.color = color;
	}

	public Integer getStockMax() {
		return stockMax;
	}

	public void setStockMax(Integer stockMax) {
		this.stockMax = stockMax;
	}

	@Override
	public String toString() {
		return "CodigoBarras [codigo=" + codigo + ", talla=" + talla + ", color=" + color + ", stockMax=" + stockMax
				+ "]";
	}


}
