package javafx.gui;


import javafx.application.Application;
import javafx.gui.controller.AppController;
import javafx.scene.image.Image;
import javafx.stage.Stage;

public class App extends Application {

	@Override
	public void start(Stage primaryStage) throws Exception {
		
		AppController controller = new AppController(primaryStage);
		controller.cambiarVista(FxmlPaths.FXML_WELCOME);
		primaryStage.getIcons().add(new Image("/img/logoIcon.jpg"));
		primaryStage.setResizable(false);
		primaryStage.show();

	}

	public static void main(String[] args) {
		launch();
	}

}