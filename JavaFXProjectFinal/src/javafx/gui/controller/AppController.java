package javafx.gui.controller;

import java.io.IOException;
import java.net.URL;
import javafx.fxml.FXMLLoader;
import javafx.gui.App;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.stage.Stage;
import services.AppJavafxServices;

public class AppController {

	private static Stage primaryStage;
	
	public static AppJavafxServices servicio = new AppJavafxServices();

	public AppController() {
	}

	public AppController(Stage stage) {
		primaryStage = stage;
	}

	public AppController cambiarVista(String fxml) {
		try {
			URL url = App.class.getResource(fxml);
			FXMLLoader loader = new FXMLLoader(url);
			Scene scene = new Scene(loader.load(), 700, 500);
			primaryStage.setScene(scene);
			return loader.getController();
		}

		catch (IOException e) {
			throw new RuntimeException("No se ha podido cargar el fxml con ruta " + fxml, e);
		}
	}

	public Parent cargarVista(String fxml) {
		try {
			URL url = App.class.getResource(fxml);
			FXMLLoader loader = new FXMLLoader(url);
			return loader.load();
		}

		catch (IOException e) {
			throw new RuntimeException("No se ha podido cargar el fxml con ruta " + fxml, e);
		}
	}

	public void errorPopUp(String mensaje) {
		Alert a = new Alert(AlertType.ERROR);
		a.setHeaderText(null);
		a.setContentText(mensaje);
		a.setTitle("Error");
		a.showAndWait();
	}
	
	
	public void informationPopUp(String mensaje) {
		Alert a = new Alert(AlertType.INFORMATION);
		a.setHeaderText(null);
		a.setContentText(mensaje);
		a.setTitle("Information");
		a.showAndWait();
		
	}

}