package javafx.gui.controller;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.gui.FxmlPaths;
import javafx.scene.control.MenuItem;
import javafx.scene.layout.BorderPane;

public class PantallaBienvenidaController extends AppController {

	private static final Logger log = LogManager.getLogger(PantallaBienvenidaController.class.getName());

	@FXML
	private BorderPane borderPanel;

	@FXML
	private MenuItem addArticulo;

	// Estos metodos se usan para llamar a las diferentes vistas 
	//o incluso salir de la app

	@FXML
	public void addArticulo(ActionEvent e) {
		borderPanel.setCenter(cargarVista(FxmlPaths.FXML_CREAR_ARTICULO));
		log.log(Level.DEBUG, "Cambiamos vista a add articulo");
	}

	@FXML
	public void addCodigoBarras(ActionEvent e) {
		borderPanel.setCenter(cargarVista(FxmlPaths.FXML_CREAR_CODIGO_BARRAS));
		log.log(Level.DEBUG, "Cambiamos vista a add codigo barras");
	}

	@FXML
	public void consultarStock(ActionEvent e) {
		borderPanel.setCenter(cargarVista(FxmlPaths.FXML_CONSULTAR_STOCK));
		log.log(Level.DEBUG, "Cambiamos vista a consultar stock de codigobarras");
	}

	@FXML
	public void consultarArticuloTabla(ActionEvent e) {
		borderPanel.setCenter(cargarVista(FxmlPaths.FXML_CONSULTAR_ARTICULO));
		log.log(Level.DEBUG, "Cambiamos vista a consultar articulo");
	}

	@FXML
	public void salirApp(ActionEvent e) {
		log.log(Level.DEBUG, "Salimos de la app");
		System.exit(0);
	}

}
