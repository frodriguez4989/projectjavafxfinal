
package javafx.gui.controller;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TextArea;
import javafx.scene.control.TextField;
import modelo.Articulo;
import services.ArticuloRepetidoException;

public class CrearArticuloController extends AppController {
	
	private static final Logger log = LogManager.getLogger(CrearArticuloController.class.getName());

	@FXML
	private TextField tfCodigoArticulo;

	@FXML
	private TextArea tfDescripcion;

	//Este metodo se activa cuando pulsamos el boton a�adir y nos comprueba 2 cosas:
	//1: Hace una consulta si previamente existe ese art�culo
	//2: Si no existe, hace un insert en la BBDD de un nuevo art�culo
	public void addFuncion(ActionEvent e) {
		if (tfDescripcion.getText().isBlank() || tfCodigoArticulo.getText().isBlank()) {
			camposVacios();
			errorPopUp("Se deben a�adir los dos campos para insertar el articulo.");
			log.log(Level.ERROR, "Error por campos vacios");
		} else {
			try {
				Articulo a = new Articulo();
				a.setCodigoArticulo(Integer.parseInt(tfCodigoArticulo.getText()));
				a.setDescripcion(tfDescripcion.getText());
				try {
					camposVacios();
					servicio.insertarArticulo(a);
					informationPopUp("Su registro de articulo se ha realizado de manera satisfactoria.");
					log.log(Level.DEBUG, "Articulo a�adido de forma satisfactoria a la BBDD");
				} catch (ArticuloRepetidoException e1) {
					camposVacios();
					errorPopUp("El codigo de articulo ya esta a�adido en BBDD, escoja otra porfavor.");
					log.log(Level.ERROR, "Error por articulo repetido en BBDD");
				}
			} catch (NumberFormatException ex) {
				camposVacios();
				errorPopUp("Recuerda que el campo codigo articulo debe ser numeral.");
				log.log(Level.ERROR, "Error por el campo codigo articulo no es numeral");
			}
		}
	}
	
	//Este metodo es usado para dejar los campos de la vista vacio
	public void camposVacios() {
		tfDescripcion.setText("");
		tfCodigoArticulo.setText("");
	}

}
