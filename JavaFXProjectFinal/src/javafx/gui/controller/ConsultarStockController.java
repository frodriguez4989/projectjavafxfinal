package javafx.gui.controller;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import modelo.CodigoBarras;
import services.CodigoBarrasNotFoundException;

public class ConsultarStockController extends AppController {

	private static final Logger log = LogManager.getLogger(ConsultarStockController.class.getName());

	@FXML
	private Button btnBuscar;

	@FXML
	private TextField tfCantidadStock;

	@FXML
	private TextField tfCodigoBarras;

	@FXML
	void buscar(KeyEvent e) {
		if (e.getCode() == KeyCode.ENTER) {
			btnBuscar.fire();
		}
	}

	// Este metodo consulta en BBDD el codigo de barras previamente, si existe te
	// mostrar� el stock de ese codigo de barras
	@FXML
	public void search(ActionEvent e) {
		if (tfCodigoBarras.getText().isBlank()) {
			errorPopUp("Introduce un codigo de barras");
			log.log(Level.ERROR, "Error campo vacio codigo barras");
		} else {
			try {
				CodigoBarras cb = servicio.consultarCodBarras(Integer.parseInt(tfCodigoBarras.getText()));
				tfCantidadStock.setText(String.valueOf(cb.getStockMax()));
				log.log(Level.DEBUG, "La consulta del codigo de barras se realizo de manera satisfactoria");
			} catch (NumberFormatException e1) {
				errorPopUp("El codigo de barras debe ser numeral");
				tfCodigoBarras.setText("");
				log.log(Level.ERROR, "Error campo codigo de barras no numeral");
			} catch (CodigoBarrasNotFoundException e1) {
				errorPopUp("El codigo de barras no existe en BBDD");
				tfCodigoBarras.setText("");
				log.log(Level.ERROR, "Error codigo de barras not found en BBDD");
			}
		}
	}

	// Este metodo limpia los campos de los textfield para un nuevo insert
	@FXML
	public void limpiar(ActionEvent e) {
		tfCantidadStock.setText("");
		tfCodigoBarras.setText("");
		log.log(Level.DEBUG, "Limpiamos campos para una nueva busqueda");
	}

}
