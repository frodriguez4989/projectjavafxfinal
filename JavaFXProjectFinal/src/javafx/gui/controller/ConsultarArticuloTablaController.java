package javafx.gui.controller;

import java.util.List;
import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.concurrent.Task;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.effect.BoxBlur;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import javafx.scene.text.Text;
import modelo.Articulo;
import modelo.CodigoBarras;

public class ConsultarArticuloTablaController extends AppController {
	
	private static final Logger log = LogManager.getLogger(ConsultarArticuloTablaController.class.getName());

	@FXML
	private TextField tfCodigoArticulo;
	
	@FXML
	private Button btnConsultar;

	@FXML
	private TableColumn<CodigoBarras, Integer> columnaCodigo;

	@FXML
	private TableColumn<CodigoBarras, String> columnaTalla;

	@FXML
	private TableColumn<CodigoBarras, String> columnaColor;

	@FXML
	private TableColumn<CodigoBarras, Integer> columnaStock;

	@FXML
	private TableView<CodigoBarras> tableArticulos;

	private ObservableList<CodigoBarras> datos;

	@FXML
	public void initialize() {
		PropertyValueFactory<CodigoBarras, Integer> factoryValueCodigo = new PropertyValueFactory<>("codigo");
		PropertyValueFactory<CodigoBarras, String> factoryValueTalla = new PropertyValueFactory<>("talla");
		PropertyValueFactory<CodigoBarras, String> factoryValueColor = new PropertyValueFactory<>("color");
		PropertyValueFactory<CodigoBarras, Integer> factoryValueStock = new PropertyValueFactory<>("stockMax");

		columnaCodigo.setCellValueFactory(factoryValueCodigo);
		columnaTalla.setCellValueFactory(factoryValueTalla);
		columnaColor.setCellValueFactory(factoryValueColor);
		columnaStock.setCellValueFactory(factoryValueStock);

		datos = FXCollections.observableArrayList();
		tableArticulos.setItems(datos);
		tableArticulos.setPlaceholder(new Text("Sin codigos de barra"));
	}
	
	@FXML
	public void consultarKey(KeyEvent e) {
		if(e.getCode()==KeyCode.ENTER) {
			btnConsultar.fire();
		}
	}
	
	@FXML
	public void consultar(ActionEvent e) {
		if (tfCodigoArticulo.getText().isBlank()) {
			errorPopUp("Ingrese un codigo articulo porfavor");
			log.log(Level.ERROR, "Error campo codigo articulo vacio");
		}
		consultar(tfCodigoArticulo.getText());
		log.log(Level.DEBUG, "Realizamos consulta a BBDD");
	}
	
	//Este metodo se encarga de hacer consulta del articulo en cuestion
	//Y mostrar los datos de sus codigos de barra, si tiene.
	//Manejamos los tasks para que la propia animacion de la app
	//Vaya por otro hilo que de la propia consulta
	//Para as� no intervenir mecanicamente en la aplicacion 
	public void consultar(String textoFiltro) {
		tfCodigoArticulo.setText(textoFiltro);
		tableArticulos.setEffect(new BoxBlur());

		Task<Void> task = new Task<Void>() {

			List<CodigoBarras> codigoBarras;

			@Override
			protected Void call() throws Exception {
				try {
					Articulo a = servicio.consultarArticulo(Integer.parseInt(textoFiltro));
					codigoBarras = a.getListaCodigoBarras();
					log.log(Level.DEBUG, "Obtenemos lista desde la BBDD");
					return null;
				} catch (NumberFormatException e) {
					errorPopUp("El campo codigo articulo debe ser numeral");
					log.log(Level.ERROR, "Error campo codigo articulo no numeral");
				}
				return null;
			}

			@Override
			protected void succeeded() {
				super.succeeded();
				tableArticulos.setEffect(null);
				datos.setAll(codigoBarras);
				log.log(Level.DEBUG, "Mostramos la lista obtenida en la app");
			}

			@Override
			protected void failed() {
				super.failed();
				tableArticulos.setEffect(null);
				datos.clear();
				errorPopUp("No hay registros con ese articulo");
				log.log(Level.ERROR, "Articulo no encontrado en bbdd");
			}

		};

		new Thread(task).start();

	}

}
