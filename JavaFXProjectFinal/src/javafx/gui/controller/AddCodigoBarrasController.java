package javafx.gui.controller;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyEvent;
import modelo.Articulo;
import modelo.CodigoBarras;
import services.ArticuloNotFoundException;
import services.CodigoBarrasRepetidoException;

public class AddCodigoBarrasController extends AppController {

	private static final Logger log = LogManager.getLogger(AddCodigoBarrasController.class.getName());

	@FXML
	private TextField tfCodigoArticulo;

	@FXML
	private TextField tfCodigoBarras;

	@FXML
	private TextField tfColor;

	@FXML
	private TextField tfStock;

	@FXML
	private TextField tfTalla;

	@FXML
	private Button btnBuscar;

	@FXML
	private Button addBtn;

	private static Articulo art;

	@FXML
	public void buscarCb(KeyEvent e) {
		if (e.getCode() == KeyCode.ENTER) {
			btnBuscar.fire();
		}
	}

	@FXML
	public void addCb(KeyEvent e) {
		if (e.getCode() == KeyCode.ENTER) {
			addBtn.fire();
		}
	}

	// Este metodo se encarga de verificar si el codigo articulo consultado existe
	// Si existe se activan todos los campos necesarios para el insert de cod barras
	@FXML
	public void buscar(ActionEvent ev) {
		if (tfCodigoArticulo.getText().isBlank()) {
			errorPopUp("Inserte un codigo articulo");
			log.log(Level.ERROR, "Error campo vacio codigo articulo");
		} else {
			try {
				art = servicio.consultarArticulo(Integer.parseInt(tfCodigoArticulo.getText()));
				informationPopUp("El articulo existe y esta disponible para la insercion de codigo de barras");
				log.log(Level.DEBUG, "El articulo existe y esta listo en BBDD");
				activarTf();
			} catch (NumberFormatException e) {
				errorPopUp("El codigo articulo debe ser numeral");
				tfCodigoArticulo.setText("");
				log.log(Level.ERROR, "Error por codigo articulo no numeral");
			} catch (ArticuloNotFoundException e) {
				errorPopUp("El articulo insertado no se encuentra en BBDD");
				tfCodigoArticulo.setText("");
				log.log(Level.ERROR, "Error por articulo no encontrado en BBDD");
			}
		}
	}

	// Este metodo activa todos los componentes necesarios para el insert del cb
	public void activarTf() {
		tfCodigoBarras.setDisable(false);
		tfColor.setDisable(false);
		tfStock.setDisable(false);
		tfTalla.setDisable(false);
		addBtn.setDisable(false);
	}

	// Este metodo recoge todos los datos introducidos previamente y posterior hace
	// el insert en BBDD
	public void addBarras(ActionEvent ev) {
		CodigoBarras cb = new CodigoBarras();
		try {
			cb.setCodigo(Integer.parseInt(tfCodigoBarras.getText()));
			cb.setColor(tfColor.getText());
			cb.setStockMax(Integer.parseInt(tfStock.getText()));
			cb.setTalla(tfTalla.getText());
			servicio.insertarCodigoBarras(art, cb);
			clear(tfCodigoBarras, tfColor, tfStock, tfTalla, tfCodigoArticulo);
			informationPopUp("El codigo de barras ha sido insertado de manera correcta");
			initialize();
			log.log(Level.DEBUG, "La insercion del codigo de barras ha sido satisfactoria");
		} catch (NumberFormatException e) {
			errorPopUp("El campo codigo y el campo stock deben ser numerales");
			clear(tfCodigoBarras, tfColor, tfStock, tfTalla);
			log.log(Level.ERROR, "Error por campos codigo y stock no numerales");
		} catch (CodigoBarrasRepetidoException e) {
			errorPopUp("El codigo de barras insertado esta repetido vuelve a rellenarlo");
			clear(tfCodigoBarras, tfColor, tfStock, tfTalla);
			log.log(Level.ERROR, "Error por codigo de barras repetido en BBDD");
		}

	}

	// Este metodo sirve para limpiar los tf que seleccionemos cuando lo llamemos
	public void clear(TextField... tf) {
		for (TextField textField : tf) {
			textField.setText("");
		}

	}

	@FXML
	public void initialize() {
		tfCodigoBarras.setDisable(true);
		tfColor.setDisable(true);
		tfStock.setDisable(true);
		tfTalla.setDisable(true);
		addBtn.setDisable(true);
		tfCodigoArticulo.requestFocus();
	}

}
