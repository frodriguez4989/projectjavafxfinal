package javafx.gui;

public class FxmlPaths {

	public static final String FXML_WELCOME = "/javafx/gui/fxml/pantallaBienvenida.fxml";
	public static final String FXML_CREAR_ARTICULO = "/javafx/gui/fxml/crearArticulo.fxml";
	public static final String FXML_CREAR_CODIGO_BARRAS = "/javafx/gui/fxml/addCodigoBarras.fxml";
	public static final String FXML_CONSULTAR_STOCK = "/javafx/gui/fxml/consultarStock.fxml";
	public static final String FXML_CONSULTAR_ARTICULO = "/javafx/gui/fxml/consultarArticuloTabla.fxml";

	
}