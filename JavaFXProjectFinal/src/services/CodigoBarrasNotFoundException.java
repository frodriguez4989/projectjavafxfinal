package services;

public class CodigoBarrasNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4386795708463832817L;

	public CodigoBarrasNotFoundException() {
	}

	public CodigoBarrasNotFoundException(String message) {
		super(message);
	}

	public CodigoBarrasNotFoundException(Throwable cause) {
		super(cause);
	}

	public CodigoBarrasNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public CodigoBarrasNotFoundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public static void main(String[] args) {

	}

}
