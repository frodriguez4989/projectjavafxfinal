package services;

public class ArticuloRepetidoException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6968714988148780059L;

	public ArticuloRepetidoException() {
	}

	public ArticuloRepetidoException(String message) {
		super(message);
	}

	public ArticuloRepetidoException(Throwable cause) {
		super(cause);
	}

	public ArticuloRepetidoException(String message, Throwable cause) {
		super(message, cause);
	}

	public ArticuloRepetidoException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public static void main(String[] args) {

	}

}
