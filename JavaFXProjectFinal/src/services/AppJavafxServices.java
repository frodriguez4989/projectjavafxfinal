package services;

import java.util.List;

import org.bson.conversions.Bson;

import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.Updates;

import config.Config;
import modelo.Articulo;
import modelo.CodigoBarras;

public class AppJavafxServices {

	private static final String DATABASE = Config.getInstance().getPropiedad("dataBase");
	private static final String COLLECTION = Config.getInstance().getPropiedad("collection");

	MongoDatabase db = new MongoUtil().getDatabase(DATABASE);
	MongoCollection<Articulo> articulo = db.getCollection(COLLECTION, Articulo.class);

	public void insertarArticulo(Articulo a) throws ArticuloRepetidoException {
		try {
			consultarArticulo(a.getCodigoArticulo());
			throw new ArticuloRepetidoException("El articulo a Insertar se encuentra repetido en BBDD");
		} catch (ArticuloNotFoundException e) {
			articulo.insertOne(a);
		}
	}

	public void insertarCodigoBarras(Articulo a, CodigoBarras codBar) throws CodigoBarrasRepetidoException {

		Bson filterArray = Filters.elemMatch("listaCodigoBarras", Filters.eq("codigo", codBar.getCodigo()));
		FindIterable<Articulo> result = articulo.find(filterArray);
		if (result.first() == null) {
			Bson filter = Filters.eq("codigoArticulo", a.getCodigoArticulo());
			Bson updates = Updates.addToSet("listaCodigoBarras", codBar);
			articulo.updateMany(filter, updates);
		} else {
			throw new CodigoBarrasRepetidoException("El codigo de barras a Insertar se encuentra repetido en BBDD");
		}

	}

	public Articulo consultarArticulo(Integer codigoArt) throws ArticuloNotFoundException {
		Bson filter = Filters.eq("codigoArticulo", codigoArt);
		FindIterable<Articulo> result = articulo.find(filter);
		Articulo a = result.first();
		if (result.first() == null) {
			throw new ArticuloNotFoundException("Articulo no encontrado en bbdd");
		}

		return a;
	}

	public CodigoBarras consultarCodBarras(Integer codigo) throws CodigoBarrasNotFoundException {
		Bson filterArray = Filters.elemMatch("listaCodigoBarras", Filters.eq("codigo", codigo));
		FindIterable<Articulo> result = articulo.find(filterArray);
		Articulo a = result.first();
		CodigoBarras cb = new CodigoBarras();
		if (a == null) {
			throw new CodigoBarrasNotFoundException("Codigo barras no encontrado en ning�n art�culo");
		} else {
			List<CodigoBarras> listaCb = a.getListaCodigoBarras();
			for (int i = 0; i < listaCb.size(); i++) {
				if (listaCb.get(i).getCodigo().equals(codigo)) {
					cb = listaCb.get(i);
				}
			}
		}
		return cb;
	}

}
