package services;

public class ArticuloNotFoundException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = -5694555533894614485L;

	public ArticuloNotFoundException() {
	}

	public ArticuloNotFoundException(String message) {
		super(message);
	}

	public ArticuloNotFoundException(Throwable cause) {
		super(cause);
	}

	public ArticuloNotFoundException(String message, Throwable cause) {
		super(message, cause);
	}

	public ArticuloNotFoundException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

}
