package services;

public class CodigoBarrasRepetidoException extends Exception {

	/**
	 * 
	 */
	private static final long serialVersionUID = 8745035525537625889L;

	public CodigoBarrasRepetidoException() {
	}

	public CodigoBarrasRepetidoException(String message) {
		super(message);
	}

	public CodigoBarrasRepetidoException(Throwable cause) {
		super(cause);
	}

	public CodigoBarrasRepetidoException(String message, Throwable cause) {
		super(message, cause);
	}

	public CodigoBarrasRepetidoException(String message, Throwable cause, boolean enableSuppression,
			boolean writableStackTrace) {
		super(message, cause, enableSuppression, writableStackTrace);
	}

	public static void main(String[] args) {

	}

}
